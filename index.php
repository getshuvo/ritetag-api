<!DOCTYPE html>
<html>
    <head>
        <title>Ritetag - Rest API</title>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="www/css/hashtagbar.css">
        <style>
            .page {padding:10px;}
        </style>
        <script src="www/js/jquery-1.11.1.js"></script>
        <script src="www/js/jquery.rest.min.js"></script>
        <script src="www/js/twitter-text.js"></script>
        <script src="www/js/q.js"></script>
        <script src="www/js/setup.js"></script>
        
        <script src="www/js/infobar.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="page">
            <h1>Hashtagbar via Ritetag rest API</h1>

            <div style="margin:15px;width: 500px;height: 200px" class="tweet-content">
                <div style="padding-left: 15px" class="ritetag-infobar"></div>
                <div style="width:100%;height: 100%;border: #292f33 1px solid" class="ritetag-richeditor" contenteditable="true"></div>
            </div>
            
        </div>

    </body>
</html>